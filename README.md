# FastCalls for Phosh

This is simple and ugly app which tries to offer faster way to place calls and browse contacts in Phosh desktop enviroment. It lives side by side with gnome-calls, gnome-contacts and modemmanager. 

Currently core functions are working and they seem reliable. I am adding more features when I find out how and I have time. 

<b>Use gui interface to add, remove or edit contacta list. It makes sure that everything stays in the correct format.</b>

You can import your all your contacts from standard vcf file using FastContacts desktop action FastImport - desktop action opens when you long press desktop icon.

## What?

Just open app and choose what you want to do. "Everything" should work - if not file an issue or mr 


![Screenshot - FastDial](screenshot1.png)


## How to install?

`cd ~ && mkdir -p .git && cd .git && git clone https://gitlab.com/Alaraajavamma/fastcalls && cd fastcalls && ./install.sh `

And reboot and it should just work

Uninstall

Long press on FastContacts desktop icon will pop-up Remove FastCalls action. After process reboot. 

Update 

Long press on FastContacts desktop icon will pop-up Update FastCalls action. After process reboot. 

## License
Feel free to do what ever you want with this but no guarantees - this will probably explode your phone xD

## Something else?
If you wan't to help or find issue feel free to contact

