#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Script must not be ran as root"
    exit 1
fi

echo "FastCalls is ment to run in Phosh enviroment. It relays on modemmanager, gnome-contacts and gnome-calls - but if you are running Phosh you have those installed and running, right?"

if command -v "pacman" >/dev/null; then
    sudo pacman -Sy yad
elif command -v "apk" >/dev/null; then
    sudo apk update
    sudo apk add yad
elif command -v "apt" >/dev/null; then
    sudo apt update
    sudo apt install -y yad
else
    echo "Unknown package manager yad needs to be install via your preferred method"
fi

mkdir -p "${HOME}/.config/autostart/"
mkdir -p "${HOME}/.local/share/applications/"
sudo mkdir -p /opt/fastcalls/yad
sudo mkdir -p /opt/fastcalls/icons
sudo mkdir -p /opt/fastcalls/db
sudo mkdir -p /opt/fastcalls/desktop
sudo ln -s "${PWD}/yad/fastcontacts.sh" "/opt/fastcalls/yad"
sudo ln -s "${PWD}/yad/fastdial.sh" "/opt/fastcalls/yad"
sudo ln -s "${PWD}/yad/fasthistory.sh" "/opt/fastcalls/yad"
sudo ln -s "${PWD}/yad/fullhistory.sh" "/opt/fastcalls/yad"
sudo ln -s "${PWD}/yad/fastimport.sh" "/opt/fastcalls/yad"
sudo ln -s "${PWD}/yad/fastexport.sh" "/opt/fastcalls/yad"
sudo ln -s "${PWD}/icons/tuxcontacts.svg" "/opt/fastcalls/icons"
sudo ln -s "${PWD}/icons/tuxdialer.svg" "/opt/fastcalls/icons"
sudo ln -s "${PWD}/db/fasthistory.txt" "/opt/fastcalls/db"
sudo ln -s "${PWD}/db/fullhistory.txt" "/opt/fastcalls/db"
sudo ln -s "${PWD}/db/fastcontacts.txt" "/opt/fastcalls/db"
sudo ln -s "${PWD}/db/combinedcontacts.txt" "/opt/fastcalls/db"
sudo ln -s "${PWD}/db/fastdialer.txt" "/opt/fastcalls/db"
sudo ln -s "${PWD}/desktop/fastcontacts.desktop" "/opt/fastcalls/desktop"
sudo ln -s "${PWD}/desktop/fastdial.desktop" "/opt/fastcalls/desktop"
sudo ln -s "${PWD}/desktop/fasthistory.desktop" "/opt/fastcalls/desktop"
sudo ln -s "${PWD}/install.sh" "/opt/fastcalls/"
sudo ln -s "${PWD}/uninstall.sh" "/opt/fastcalls/"
sudo ln -s "${PWD}/update.sh" "/opt/fastcalls/"
sudo ln -s "${PWD}/README.md" "/opt/fastcalls/"
sudo ln -s "${PWD}/screenshot1.png" "/opt/fastcalls/"
ln -s "${PWD}/desktop/fastcontacts.desktop" "${HOME}/.local/share/applications/fastcontacts.desktop"
ln -s "${PWD}/desktop/fastdial.desktop"  "${HOME}/.local/share/applications/fastdial.desktop"
ln -s "${PWD}/desktop/fasthistory.desktop" "${HOME}/.config/autostart/fasthistory.desktop"
