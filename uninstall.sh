#!/bin/bash

rm ~/.local/share/applications/fastdial.desktop
rm ~/.local/share/applications/fastcontacts.desktop
rm ~/.config/autostart/fasthistory.desktop

cd ~ && rm -rf .git/fastcalls

pkexec rm -rf /opt/fastcalls
