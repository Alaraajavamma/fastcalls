#!/bin/bash
cd ~/.git/fastcalls && rm ~/.git/fastcalls/db/combinedcontacts.txt


# Make sure that we have necessary database file in place
if [ ! -f ~/.git/fastcalls/db/fastcontacts.txt ]; then
    touch ~/.git/fastcalls/db/fastcontacts.txt
fi


# Your contacts file
CONTACTS_FILE=~/.git/fastcalls/db/fastcontacts.txt

# Path to the temporary combined file
COMBINED_FILE=~/.git/fastcalls/db/combinedcontacts.txt

# Function to combine two rows and remove them from the contacts file
function combine_and_remove_rows() {
  local name1
  local number1
  local number2
  
  while IFS= read -r name1 && IFS= read -r number1 && IFS= read -r _ && IFS= read -r number2; do
    echo "<b>$name1</b>","$number1","$number2" >> tmp2
  done < "$CONTACTS_FILE"
  
  sort tmp2 | awk -F "," '{print $1 "\n" $2 "\n" $3}' > $COMBINED_FILE
  rm tmp2
}

function combine_remove () {
rm tmp3 2>/dev/null || true
local name1
  
  while IFS= read -r name1 && IFS= read -r _ && IFS= read -r _ && IFS= read -r _; do
    echo "$name1" >> tmp3
  done < "$CONTACTS_FILE"
  
  sort tmp3 > $COMBINED_FILE
  rm tmp3
}
# Function to display the contact list
function display_contacts() {
  local selected_contact
  selected_contact=$(cat "$COMBINED_FILE" | yad --title "FastContacts" \
      --list --column="Contact" \
      --grid-lines=both \
      --ellipsize=END \
      --text="Type to search
To call select number field" \
      --text-align=center \
      --search-column=1 \
      --buttons-layout=center \
      --button="Call 📞":2 \
      --button="Remove🗑️ or edit✍️":3 \
      --no-headers \
      --regex-search)

case $? in
	2)
	call_contact "$selected_contact"
	;;
	3)
	choose_contacts "$selected_contact"
	;;
	0)
  call_contact "$selected_contact"
	;;
	esac
}

# Function to initiate a call
function call_contact() {
Number=$(echo "$selected_contact" | awk -F '|' '{print $1}')
  echo $Number
# Hide squeekboard before call starts
busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b false

  # Initiate the call using ModemManager
  mmcli -m any --voice-create-call="number=$Number"
}

#Function to choose edit or remove
function choose_contacts() {
(yad --title "FastContacts Choose" \
      --text="Do you want to edit or delete contacts?" \
      --text-align=center \
      --buttons-layout=center \
      --button="Remove 🗑️":2 \
      --button="Edit ✍️":3 )

case $? in
        2) 
        combine_remove
        rem_contacts
        ;;
        3)
        combine_remove
        edit_contacts
        ;;
	esac
}


#Function to display the contact list for removing purpose
function rem_contacts() {
  local selected_contact
  selected_contact=$(cat "$COMBINED_FILE" | yad --title "FastContacts remove" \
      --list --column="Contact" \
      --grid-lines=both \
      --text="Select contact and press remove - <b>be carefull because you can't undo removing</b>
You can search contacts by typing" \
      --search-column=1 \
      --ellipsize=END \
      --text-align=center \
      --buttons-layout=center \
      --button="Remove contact 🗑️":2 \
      --regex-search)

case $? in
        2) 
        remove_contacts "$selected_contact"
        ;;
	esac
}

#Function to display the contact list for editing purpose
function edit_contacts() {
  local selected_contact
  selected_contact=$(cat "$COMBINED_FILE" | yad --title "FastContacts edit" \
      --list --column="Contact" \
      --grid-lines=both \
      --text="Select contact and press edit - <b>it will open edit window - be carefull you can't undo changes</b>
You can search contacts by typing" \
      --text-align=center \
      --ellipsize=END \
      --search-column=1 \
      --buttons-layout=center \
      --button="Edit contact ✍️":2 \
      --regex-search)

case $? in
        2) 
        remove_contacts "$selected_contact" && add_contact
        ;;
	esac
}


# Open new window to edit contact
function add_contact() {
Name=$(echo "$selected_contact" | awk -F '|' '{print $1}')
  echo $Name
  echo $editnumber
  output=$(yad --title "Edit FastContact" \
             --text="To <b>Name</b> field use format Firsname Lastname
To <b>Country code</b> field put only the country code with + mark (example +358).
To <b>Number</b> field put phonenumber without country code but with leading zero (example 0400123456)
<b>Do not stop this process or currently edited contact will be gone forever</b>" \
             --form \
             --field="Name":Example "$Name"\
             --field="Country code":Example +\
             --field="Number":Example "$editnumber" \
             --buttons-layout=center \
             --button="Add edited contact 📇":2)

case $? in
    2)
      Name=$(echo "$output" | awk -F '|' '{print $1}')
      Country_code=$(echo "$output" | awk -F '|' '{print $2}')
      Number=$(echo "$output" | awk -F '|' '{print $3}')
      echo $Name >> $CONTACTS_FILE
      echo $Number >> $CONTACTS_FILE
      echo $Name >> $CONTACTS_FILE
      echo $Country_code$Number >> $CONTACTS_FILE
      ;; 
  esac
}

# Function to remove contact
function remove_contacts() {
Name=$(echo "$selected_contact" | awk -F '|' '{print $1}')
  echo $Name
  
  search_term="$Name"
  filename="$CONTACTS_FILE"

  row_number=$(grep -xnw "$search_term" "$filename" | cut -d ':' -f 1 | head -n 1)
  if [ -n "$row_number" ]; then
    echo "Match found at row: $row_number"
    editrow=$((row_number + 1))
    editnumber=$(sed -n "${editrow}p" "$filename")

    # Process the first match only
    sed -i "${row_number}d;$((row_number + 1))d;$((row_number + 2))d;$((row_number + 3))d" "$filename"

    # Process the rest of the function...
  
  else
    echo "No match found"
  fi
}

# Combine two rows and remove them from the contacts file
combine_and_remove_rows

# Display the initial contact list
display_contacts
