#!/bin/bash

# File path variable
FILE_PATH=~/.git/fastcalls/db/fastcontacts.txt
TEMP_FILE=~/.git/fastcalls/db/tmp11.txt

# Output file for VCF
VCF_FILE=~/fastexport.vcf

# Process the file and create VCF
{
    while IFS= read -r name && IFS= read -r number && IFS= read -r repeated_name && IFS= read -r country_number; do
        if [[ $name != 📦* ]]; then
            # Add contact to VCF
            echo "BEGIN:VCARD"
            echo "VERSION:3.0"
            echo "FN:$name"
            echo "TEL;TYPE=HOME,VOICE:$number"
            echo "END:VCARD"

            # Mark as exported in temp file
            echo "📦$name" >> "$TEMP_FILE"
            echo "$number" >> "$TEMP_FILE"
            echo "📦$repeated_name" >> "$TEMP_FILE"
            echo "$country_number" >> "$TEMP_FILE"
        else
            # Keep unmodified contact in temp file
            echo "$name" >> "$TEMP_FILE"
            echo "$number" >> "$TEMP_FILE"
            echo "$repeated_name" >> "$TEMP_FILE"
            echo "$country_number" >> "$TEMP_FILE"
        fi
    done
} < "$FILE_PATH" > "$VCF_FILE"

# Replace original file with updated temp file
mv "$TEMP_FILE" "$FILE_PATH"
