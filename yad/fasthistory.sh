#!/bin/bash
cd ~/.git/fastcalls

# Path to the output file for phone numbers
PHONE_NUMBER_FILE=~/.git/fastcalls/db/fasthistory.txt

# Path for full history txt file
FULL_HISTORY=~/.git/fastcalls/db/fullhistory.txt

# Monitor for incoming calls
monitor() {
dbus-monitor --system "interface='org.freedesktop.ModemManager1.Modem.Voice',type='signal',member='CallAdded'" |
		while read -r line; do
			echo "$line" | grep -qE "^signal.*CallAdded" && callid
			done
}

#Monitor callid so we can check phonenumber and call state 
callid() { TIME=$(date +"%H:%M")
	DAY=$(date +"%-d.%-m.")
	VOICECALLID="$(
    output=$(mmcli -m any --voice-list-calls -a)
    number=$(echo "$output" | awk -F'/Call/' '{print $2}' | awk '{print $1}')
    echo $number
)"
phone
}

# Monitor other partys phonenumber
phone() {
	PHONENUMBER="$(mmcli -m any -o "$VOICECALLID" -K | \
		grep call.properties.number | \
		cut -d ':' -f2 | \
		tr -d  ' ')"
		
		direct
}

# Monitor call direction (outgoing or incoming)
direct () { 
	DIRECTION="$(
		mmcli --voice-status -o "$VOICECALLID" -K |
		grep call.properties.direction |
		cut -d: -f2 |
		tr -d " "
	)"
	echo $DIRECTION
	changes
}


# Monitor for call changes
# Initialize variables
changes() {
start_time=0
end_time=0

while true; do

 # Check the value of $ACTIVE
    ACTIVE="$(mmcli --voice-status -o "$VOICECALLID" -K |
                grep -w "call.properties.state" |
                cut -d: -f2 |
                tr -d " " |
                tr -cd '[[:alpha:]]'
            )"
            
    if [[ $ACTIVE == "active" || $ACTIVE == "activeaccepted" ]]; then
    if (($start_time == 0)); then
        start_time=$(date +%s)
        feedbackd_profile=$(gsettings get org.sigxcpu.feedbackd profile | tr -d "'")
        gsettings set org.sigxcpu.feedbackd profile silent
        fi
    elif [[ $ACTIVE == "error*" || $ACTIVE == "" ]]; then
    if (($end_time == 0)); then
    end_time=$(date +%s)
        gsettings set org.sigxcpu.feedbackd profile $feedbackd_profile
        mmcli -m any -o $VOICECALLID --hangup
        mmcli -m any --voice-delete-call=$VOICECALLID
        break
        fi
    fi

    sleep 1
done
    if (($start_time == 0)); then
    duration=$"🔴NoAnswer"
    else
    dur=$((end_time - start_time))
    #format
    dura=$(date -d@$dur -u +%M:%S)
    duration="🟢$dura"
    fi
    
parse
}

# Parse information to fasthistory format
parse() {
# Shorten the direction word
if [ "$DIRECTION" == "incoming" ]; then
  short="📲IN"
elif [ "$DIRECTION" == "outgoing" ]; then
  short="🚀OUT"
else
  short="?"
fi

timestamp="🕘$TIME 📆$DAY $short $duration"

# Format the output with timestamp on the first row and number on the second row
                output="$timestamp\n$PHONENUMBER"

                # Export the output to the PHONE_NUMBER_FILE
                echo -e "$output" >> "$PHONE_NUMBER_FILE"
                rowcutter
                
}

rowcutter() {
# Check total row count
total_rows=$(wc -l < $PHONE_NUMBER_FILE)
max_rows=100

# Move upper rows to full history file if row count is more than 99
if [[ $total_rows -gt $max_rows ]]; then
mover=$(($total_rows - $max_rows)) 

# Move rows to FULL_HISTORY so we can keep the max_rows and fastcalls fresh fast
head -n $mover $PHONE_NUMBER_FILE >> $FULL_HISTORY

# Delete the upper rows
tail -n $max_rows $PHONE_NUMBER_FILE >> tmpfile && mv tmpfile $PHONE_NUMBER_FILE
fi
}

# Main script
monitor
