#!/bin/bash

# Your contacts file
CONTACTS_FILE=~/.git/fastcalls/db/fastcontacts.txt

function choose_vcf() {
  selected_vcf=$(cd ~ && ls *.vcf | yad --title "FastImport" \
    --list --column="vcf" \
    --text "Choose vcf file
Files are searched from your home folder
If there are no options, export your contacts to the home folder using gnome-contacts
This action will take some time; please be patient" \
    --no-headers \
    --text-align=center \
    --buttons-layout=center \
    --button="Import contacts 📇":2 )
      
  case $? in
    2)
      if [[ -z $selected_vcf ]]; then 
        notify-send Error "No vcf file selected; aborting" -i face-sad-symbolic
      else
        rename_vcf "$selected_vcf"
      fi
      ;;
  esac 
}

function rename_vcf() {
  file=$(echo "$1" | tr -d '|')
  echo "$file"
  # Specify the path to your vcf file
  VCF_FILE=~/"$file"
  echo "$VCF_FILE"
  start "$VCF_FILE"
}

function start() {
notify-send Importing "Sorry this is kind of slow but we are running as fast as we can - please wait" -i face-smile-big-symbolic

  local VCF_FILE="$1"
  # Declare associative arrays for names and phone numbers
  declare -A names
  declare -A phone_numbers

  # Process the vcf file
  while IFS= read -r line; do
    if [[ $line == "FN:"* ]]; then
name=$(echo "$line" | grep -o 'FN:[^:]*' | awk -F':' '{gsub(/[[:space:]]*$/, "", $2); print $2}')
extracted_name="📦$name"

    elif [[ $line == "TEL;"* ]]; then
    phone_number=$(echo "$line" | grep -o 'E164=[0-9]*' | awk -F'=' '{print $2}')

# Extract the phone number and add '0' before saving it
extracted_phone_number="0$phone_number"
  
  phone_numbers["$extracted_name"]=$extracted_phone_number
    fi
  done < "$VCF_FILE"
  cont
}

function cont() {
  # Print all names and phone numbers
  for name in "${!phone_numbers[@]}"; do
    echo "$name","${phone_numbers[$name]}","$name","${phone_numbers[$name]}" >> tmp7
  done
  sort tmp7 | awk -F "," '{print $1 "\n" $2 "\n" $3 "\n" $4}' > tmp8
  choose_country_code
}

function choose_country_code() {
  input=$(yad --title="Input country code" \
    --form \
    --field="Country code":Example +\
    --text "Input default country code in the format +358" \
    --buttons-layout=center \
    --button="Input":2)

  case $? in
    2)
      edit "$input"
      ;; 
  esac
}

function edit() {
  local input="$1"
  local Country_code=$(echo "$input" | awk -F '|' '{print $1}')
  local Digits=${#Country_code}
  
  while IFS= read -r name1 && IFS= read -r number1 && IFS= read -r name2 && IFS= read -r number2; do
    echo "$name1" >> tmp9
    if [[ $number1 == $Country_code* ]]; then 
          number="0${number1:$Digits}"
      echo "$number" >> tmp9 
    else  
      echo "$number1" >> tmp9
    fi
    echo "$name2" >> tmp9
    if [[ $number2 == 0* ]]; then 
      number="$Country_code${number2:1}"
      echo "$number" >> tmp9 
    else 
      echo "$number2" >> tmp9
    fi
  done < tmp8

  cat tmp9 >> "$CONTACTS_FILE"
  
  rm tmp7
  rm tmp8
  rm tmp9
}

# main script
choose_vcf
