#!/bin/bash
cd ~/.git/fastcalls

# Make sure that we have necessary database file in place
if [ ! -f ~/.git/fastcalls/db/fastcontacts.txt ]; then
    touch ~/.git/fastcalls/db/fastcontacts.txt
fi


# Path to the call history file
CALL_HISTORY_FILE=~/.git/fastcalls/db/fullhistory.txt

# Path to the contacts file
CONTACTS_FILE=~/.git/fastcalls/db/fastcontacts.txt

# Path to the temporary combined file
COMBINED_FILE=~/.git/fastcalls/db/fastdialer.txt


# Function to fetch the contact name for a given phone number
get_contact_name() {
    local phone_number=$1
        if [[ ! $phone_number =~ ^\+?[0-9]+$ ]]; then
        echo "Hidden number"
    else

    local contact_name=$(grep -B 1 -w "$phone_number" "$CONTACTS_FILE" | head -n 1)
    echo "${contact_name:-Unknown Caller}"
    fi
}

# Function to combine the call history and contacts into a temporary file
combine_call_history_contacts() {
    local call_history=$1
    local combined_output=""

    while IFS= read -r timestamp; do
        read -r phone_number
        contact_name=$(get_contact_name "$phone_number")
        combined_output+="\n$timestamp\n$phone_number\n<b>$contact_name</b>"
    done < "$call_history"

    # Create the combined file
    echo -e "$combined_output" > txt5
    sed '/^[[:space:]]*$/d' txt5 > "$COMBINED_FILE"
    rm txt5
}  

function display_call_history() {
  local selected_contact
  selected_contact=$(tac "$COMBINED_FILE" | yad --title "FastCall History" \
      --list --column="Number" \
      --text="Choose number field to call or to add to contacts" \
      --print-column=2 \
      --no-headers \
      --text-align=center \
      --buttons-layout=center \
      --button="Call 📞":8 \
      --button="Add contact 📇":2 )
      
  case $? in
    8)
      call_contact "$selected_contact"
      ;;
    0)
      call_contact "$selected_contact"
      ;;
    2)
    if [[ -z "$selected_contact" ]]; then
    echo $selected_contact
      add_contact 
      else
      add_number_to "$selected_contact"
      fi
      ;;
  esac 
 }

# Function to initiate a call
function call_contact() {
  number=$(echo "$selected_contact" | sed 's/.$//')
  if [[ -z $number ]]; then 
  dial_call
  else
# Hide squeekboard before call starts
busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b false

  # Initiate the call using ModemManager
  mmcli -m any --voice-create-call="number=$number"
  fi
}

# Function to start dialing
function dial_call() {
input=$(yad --title="FastCall dial" \
        --entry \
        --text "Dial and call" \
        --buttons-layout=center \
        --button="Call 📞":2)

case $? in
    2)
    # Hide squeekboard before call starts 
busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b false
      mmcli -m any --voice-create-call="number=$input"
      ;; 
  esac
}

# Function to add contact to addressbook
function add_contact() {
  output=$(yad --title "Add to contacts" \
             --text="To <b>Name</b> field use format Firsname Lastname
To <b>Country code</b> field put only the country code with + mark (example +358).
To <b>Number</b> field put phonenumber without country code but with leading zero (example 0400123456)" \
             --form \
             --field="Name":Example Name \
             --field="Country code":Example +\
             --field="Number":Example 0 \
             --buttons-layout=center \
             --button="Add to contacts 📇":2)

case $? in
    2)
      Name=$(echo "$output" | awk -F '|' '{print $1}')
      Country_code=$(echo "$output" | awk -F '|' '{print $2}')
      Number=$(echo "$output" | awk -F '|' '{print $3}')
      if grep -q "$Number" $CONTACTS_FILE; then
      notify-send Error "Number is all ready added to FastContacts" -i face-sad-symbolic
      else
      echo $Name >> $CONTACTS_FILE
      echo $Number >> $CONTACTS_FILE
      echo $Name >> $CONTACTS_FILE
      echo $Country_code$Number >> $CONTACTS_FILE
      fi
      ;; 
  esac
}


# Function to add number to contacts
function add_number_to() {
number=$(echo "$selected_contact" | sed 's/.$//')

input=$(yad --title="FastCall add contact" \
            --text="We are adding <b>$number</b> to contacts
To <b>Name</b> field use format Firsname Lastname
To <b>Country code</b> field put only the country code with + mark (example +358)." \
             --form \
             --field="Name":Example Name \
             --field="Country code":Example +\
        --buttons-layout=center \
        --button="Add to contacts 📇":2)

case $? in
    2)
      Name=$(echo "$input" | awk -F '|' '{print $1}')
      Country_code=$(echo "$input" | awk -F '|' '{print $2}')
      Digits=${#Country_code}
      if [[ $number == $Country_code* ]]; then
      # Convert "+358" numbers to "0"
      converted_number="0${number:$Digits}"
     
      if grep -q "$number" $CONTACTS_FILE; then
      notify-send Error "Number is all ready added to FastContacts" -i face-sad-symbolic
      else
      
      echo $Name >> $CONTACTS_FILE
      echo $converted_number >> $CONTACTS_FILE
      echo $Name >> $CONTACTS_FILE
      echo $number >> $CONTACTS_FILE
      fi
      fi
      
    if [[ $number == 0* ]]; then
      # Convert "0" numbers to "+358"
      converted_number="$Country_code${number:1}"
      if grep -q "$number" $CONTACTS_FILE; then
      notify-send Error "Number is all ready added to FastContacts" -i face-sad-symbolic
      else
      echo $Name >> $CONTACTS_FILE
      echo $number >> $CONTACTS_FILE
      echo $Name >> $CONTACTS_FILE
      echo $converted_number >> $CONTACTS_FILE
    fi
    fi
      ;; 
  esac
}

# Main script
combine_call_history_contacts "$CALL_HISTORY_FILE"
display_call_history

# Clean up the temporary combined file
rm "$COMBINED_FILE"
